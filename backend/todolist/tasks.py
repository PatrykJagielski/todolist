from __future__ import absolute_import, unicode_literals

from celery import shared_task
from .models import Employee


@shared_task
def add_employee(data):
    Employee.objects.create(**data)
