from datetime import date, timedelta
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Category, Employee, Task


class EmployeeTestCase(APITestCase):
    def setUp(self):
        self.employees = Employee.objects.bulk_create([
            Employee(first_name='Adam', last_name='Małysz'),
            Employee(first_name='Kamil', last_name='Stoch'),
            Employee(first_name='Piotr', last_name='Żyła'),
        ])

    def test_employee_list(self):
        url = reverse('employee')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Employee.objects.count(), len(response.data))
        self.assertEqual(
            self.employees[0].first_name,
            response.data[0].get('first_name')
        )
        self.assertEqual(
            self.employees[0].last_name,
            response.data[0].get('last_name')
        )

    def test_employee_create(self):
        url = reverse('employee')
        data = {
            'first_name': 'Wojciech',
            'last_name': 'Skupień',
        }
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(Employee.objects.count(), len(self.employees) + 1)
        self.assertEqual(response.data.get('first_name'), data['first_name'])
        self.assertEqual(response.data.get('last_name'), data['last_name'])

    def test_employee_retrieve(self):
        employee = Employee.objects.create(first_name='Adam', last_name='Last')
        category = Category.objects.create(name='test category')
        tasks = Task.objects.bulk_create([
            Task(status='IP', assign_to=employee, description='test1', category=category),
            Task(status='IP', assign_to=employee, description='test2', category=category),
            Task(status='DO', assign_to=employee, description='test3', category=category),
        ])
        url = reverse('employee-detail', kwargs={'pk': employee.id})
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data.get('tasks')), len(tasks))
        self.assertEqual(response.data.get('first_name'), employee.first_name)
        self.assertEqual(response.data.get('last_name'), employee.last_name)


class TaskTestCase(APITestCase):
    def setUp(self):
        self.employee = Employee.objects.create(first_name='T', last_name='Tt')
        self.category = Category.objects.create(name='test category')

    def test_task_create(self):
        data = {
            'status': 'IP',
            'assign_to': self.employee.id,
            'description': 'test',
            'category': self.category.id,
            'deadline': '2022-08-28'
        }
        url = reverse('task-create')
        response = self.client.post(url, data)
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data.get('status'), data.get('status'))
        self.assertEqual(response.data.get('assign_to'), data.get('assign_to'))
        self.assertEqual(response.data.get('description'), data.get('description'))
        self.assertEqual(response.data.get('category'), data.get('category'))
        self.assertEqual(response.data.get('deadline'), data.get('deadline'))
        self.assertEqual(Task.objects.count(), 1)
        self.assertTrue(
            response.data.get('id') in self.employee.tasks.values_list('id', flat=True)
        )

    def test_task_destroy(self):
        task = Task.objects.create(status='IP', assign_to=self.employee,
                                   description='test', category=self.category)
        url = reverse('task-destroy-update', kwargs={'pk': task.id})
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(Task.objects.count(), 0)

    def test_task_update_status(self):
        task = Task.objects.create(status='IP', assign_to=self.employee,
                                   description='test', category=self.category)
        url = reverse('task-destroy-update', kwargs={'pk': task.id})
        data = {'status': 'DO'}
        response = self.client.patch(url, data)
        task.refresh_from_db()
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data.get('status'), data.get('status'))
        self.assertEqual(task.status, data.get('status'))

    def test_task_PUT_method_not_allowed(self):
        task = Task.objects.create(status='IP', assign_to=self.employee,
                                   description='test', category=self.category)
        url = reverse('task-destroy-update', kwargs={'pk': task.id})
        data = {'status': 'DO'}
        response = self.client.put(url, data)
        self.assertEqual(response.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

    def test_list_employees_and_today_tasks(self):
        employee1 = Employee.objects.create(first_name='T1', last_name='T1')
        employee2 = Employee.objects.create(first_name='T2', last_name='T2')
        category = Category.objects.create(name='test category')
        Task.objects.bulk_create([
            Task(status='IP', assign_to=employee1, description='test1',
                 category=category, deadline=date.today()),
            Task(status='IP', assign_to=employee1, description='test1',
                 category=category, deadline=date.today() + timedelta(days=1)),
            Task(status='IP', assign_to=employee2, description='test1',
                 category=category, deadline=date.today()),
            Task(status='IP', assign_to=employee2, description='test1',
                 category=category, deadline=date.today() + timedelta(days=1))
        ])
        url = reverse('employees-and-today-tasks')
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(Employee.objects.count(), len(response.data))
        task_count = 0
        for employee in response.data:
            task_count += len(employee['tasks'])
        self.assertEqual(Task.objects.filter(deadline=date.today()).count(), task_count)


class TaskFilterTestCase(APITestCase):
    def setUp(self):
        self.employee = Employee.objects.create(first_name='T', last_name='Tt')
        self.category = Category.objects.create(name='test category')
        self.tasks = Task.objects.bulk_create([
            Task(status='IP', assign_to=self.employee, description='test1',
                 category=self.category, deadline='2022-08-23'),
            Task(status='IP', assign_to=self.employee, description='test2',
                 category=self.category, deadline='2022-08-24'),
            Task(status='DO', assign_to=self.employee, description='test3',
                 category=self.category, deadline='2022-08-25'),
            Task(status='DO', assign_to=self.employee, description='test3',
                 category=self.category, deadline='2022-08-26'),
        ])
        self.url = reverse('employee-tasks', kwargs={'pk': self.employee.id})

    def test_filter_by_status(self):
        query_params = {'status': 'IP'}
        response = self.client.get(self.url, query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.employee.tasks.filter(**query_params).count(),
            len(response.data)
        )

    def test_filter_by_deadline(self):
        query_params = {
            'deadline__gte': '2022-08-24',
            'deadline__lte': '2022-08-25'
        }
        response = self.client.get(self.url, query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.employee.tasks.filter(**query_params).count(),
            len(response.data)
        )

    def test_filter_by_deadline_and_status(self):
        query_params = {
            'deadline__gte': '2022-08-24',
            'deadline__lte': '2022-08-25',
            'status': 'IP'
        }
        response = self.client.get(self.url, query_params)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            self.employee.tasks.filter(**query_params).count(),
            len(response.data)
        )
