from rest_framework.serializers import ModelSerializer
from .models import Category, Employee, Task


class EmployeeListSerializer(ModelSerializer):
    class Meta:
        model = Employee
        fields = ('id', 'first_name', 'last_name',)


class CategorySerializer(ModelSerializer):
    class Meta:
        model = Category
        fields = ('id', 'name')


class TaskSerializer(ModelSerializer):
    category = CategorySerializer()

    class Meta:
        model = Task
        fields = ('id', 'status', 'description', 'category', 'deadline',)


class EmployeeRetrieveSerializer(ModelSerializer):
    tasks = TaskSerializer(many=True)

    class Meta:
        model = Employee
        fields = ('first_name', 'last_name', 'tasks',)


class TaskCreateSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = ('id', 'status', 'assign_to', 'description', 'category',
                  'deadline',)


class TaskStatusSerializer(ModelSerializer):
    class Meta:
        model = Task
        fields = ('status',)
