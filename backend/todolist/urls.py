from django.urls import path
from .views import EmployeeApiView, EmployeeRetrieveApiView, TaskCreateApiView, \
    TaskDestroyUpdateApiView, EmployeeTasksList, CategoryListApiView, \
    EmployeesAndTodayTasks, export_task_to_csv, AsyncAddEmployee


urlpatterns = [
    path('employee/', EmployeeApiView.as_view(), name='employee'),
    path('employee/<pk>/', EmployeeRetrieveApiView.as_view(),
         name='employee-detail'),
    path('task/', TaskCreateApiView.as_view(), name='task-create'),
    path('task/<pk>/', TaskDestroyUpdateApiView.as_view(),
         name='task-destroy-update'),
    path('employee-tasks/<pk>/', EmployeeTasksList.as_view(),
         name='employee-tasks'),
    path('category/', CategoryListApiView.as_view(), name='category'),
    path('employees-and-tasks/', EmployeesAndTodayTasks.as_view(),
         name='employees-and-today-tasks'),
    path('tasks-csv/<pk>/', export_task_to_csv, name='tasks-csv'),
    path('async-add-employee/', AsyncAddEmployee.as_view(),
         name='async-add-employee'),
]
