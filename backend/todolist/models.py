from django.db import models


class Employee(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)

    def __str__(self):
        return f'{self.first_name} {self.last_name}'


class Category(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name_plural = "categories"


class Task(models.Model):
    STATUS = [
        ('NA', 'Not Assigned'),
        ('IP', 'In Progress'),
        ('DO', 'Done'),
    ]
    status = models.CharField(max_length=2, choices=STATUS, default='NA')
    assign_to = models.ForeignKey(
        Employee,
        on_delete=models.SET_NULL,
        related_name='tasks',
        related_query_name='task',
        null=True, blank=True
    )
    description = models.TextField(max_length=1000)
    category = models.ForeignKey(
        Category,
        on_delete=models.SET_NULL,
        null=True, blank=True
    )
    deadline = models.DateField(null=True, blank=True)
