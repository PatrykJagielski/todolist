from datetime import date
from django.db.models import Prefetch
from django_filters.rest_framework import DjangoFilterBackend
from rest_framework.generics import ListCreateAPIView, RetrieveAPIView, \
    CreateAPIView, DestroyAPIView, UpdateAPIView, ListAPIView
from drf_yasg.utils import swagger_auto_schema
from rest_framework.views import APIView
from rest_framework.decorators import api_view, renderer_classes
from rest_framework_csv.renderers import CSVRenderer
from rest_framework.response import Response
from rest_framework import status
from .models import Category, Employee, Task
from .serializers import EmployeeListSerializer, EmployeeRetrieveSerializer, \
    TaskCreateSerializer, TaskSerializer, TaskStatusSerializer, \
    CategorySerializer
from todolist import tasks


class EmployeeApiView(ListCreateAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeListSerializer


class EmployeeRetrieveApiView(RetrieveAPIView):
    queryset = Employee.objects.all()
    serializer_class = EmployeeRetrieveSerializer


class TaskCreateApiView(CreateAPIView):
    serializer_class = TaskCreateSerializer


class TaskDestroyUpdateApiView(DestroyAPIView, UpdateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskStatusSerializer
    http_method_names = ['patch', 'delete']


class EmployeeTasksList(ListAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    filter_backends = [DjangoFilterBackend]
    filterset_fields = {
        'status': ['exact'],
        'deadline': ['gte', 'lte']
    }

    def get_queryset(self):
        queryset = super().get_queryset()
        return queryset.filter(assign_to=self.kwargs['pk'])


class CategoryListApiView(ListAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer


class EmployeesAndTodayTasks(ListAPIView):
    queryset = Employee.objects.prefetch_related(
        Prefetch('tasks', queryset=Task.objects.filter(deadline=date.today()))
    )
    serializer_class = EmployeeRetrieveSerializer


@api_view(['GET'])
@renderer_classes((CSVRenderer,))
def export_task_to_csv(request, pk):
    print(request)
    tasks = Task.objects.filter(assign_to=pk)
    content = [{'description': task.description,
                'deadline': task.deadline,
                'status': dict(Task.STATUS).get(task.status),
                'category': task.category.name if task.category else None}
               for task in tasks]
    return Response(content)


class AsyncAddEmployee(APIView):
    @swagger_auto_schema(
        operation_description="Async Add Employee",
        responses={202: 'Accepted'},
        query_serializer=EmployeeListSerializer)
    def post(self, request):
        serializer = EmployeeListSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        tasks.add_employee.delay(serializer.data)
        return Response(status=status.HTTP_202_ACCEPTED)
