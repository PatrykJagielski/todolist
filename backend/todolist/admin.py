from django.contrib import admin
from .models import Category, Employee ,Task


class TaskAdmin(admin.ModelAdmin):
    list_display = ('id', 'status', 'assign_to', 'category', 'deadline',)
    list_filter = ('status',)


admin.site.register(Employee)
admin.site.register(Category)
admin.site.register(Task, TaskAdmin)