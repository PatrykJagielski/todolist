# Start command
`docker-compose up --build`

Live demo at: [patrykjagielski.pl](patrykjagielski.pl)

endpoint zwracający listę wszystkich pracowników wraz z ich zadaniami na dziś: [drf.patrykjagielski.pl/todo/employees-and-tasks/](drf.patrykjagielski.pl/todo/employees-and-tasks/)
