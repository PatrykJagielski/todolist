$(function() {
  $.urlParam = (name) => {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
                      .exec(window.location.search);
    return (results !== null) ? results[1] || 0 : false;
  }
  const STATUS = {
    'IP': 'w toku',
    'DO': 'ukończone'
  }
  const BACKEND_URL = 'http://localhost:8000/todo/';
  var id = $.urlParam('id');
  var $tasks = $('#accordionTasks');
  var $category = $('#category');
  var categoryMap = {};

  const addTask = (id, status, description, category, deadline) => {
    $tasks.append(
      `<div class="accordion-item" id=${id}>
         <h2 class="accordion-header" id="heading${id}">
           <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse" data-bs-target="#collapse${id}" aria-expanded="false" aria-controls="collapse${id}">
             ${description}
           </button>
         </h2>
         <div id="collapse${id}" class="accordion-collapse collapse" aria-labelledby="heading${id}" data-bs-parent="#accordionTasks">
           <div class="accordion-body">
             <h6 id="status-${id}">status: ${STATUS[status]}</h6>
             <h6>kategoria: ${category ? category.name : null}</h6>
             <h6>deadline: ${deadline}</h6>
             <button type="button" class="btn btn-danger delete" id="${id}" data="test">Usuń</button>
             ${status !== 'DO' ? 
               `<button type="button" class="btn btn-primary done" id="${id}" data="test">Zakończ</button>`
               : ''}
           </div>
         </div>
       </div>`
    );
  }

  const addCategory = (id, name) => {
    $category.append(`<option value=${id}>${name}</option>`);
    categoryMap[id] = {id: id, name: name};
  }

  // get employee and tasks
  $.ajax({
    type: 'GET',
    url: BACKEND_URL + `employee/${id}/`,
    success: ({first_name, last_name, tasks}) => {
      $('.container').prepend(`<h1>${first_name} ${last_name}</h1>`);
      $.each(tasks, (i, {id, status, description, category, deadline}) => {
        addTask(id, status, description, category, deadline);
      });
    },
    error: (e) => {
      console.log(e);
    }
  });

  // get categories
  $.ajax({
    type: 'GET',
    url: BACKEND_URL + 'category/',
    success: (categories) => {
      $.each(categories, (i, {id, name}) => {
        addCategory(id, name);
      });
    },
    error: (e) => {
      console.log(e);
    }
  });

  // delete task
  $(document).on('click', '.delete', (button) => {
    $.ajax({
      type: 'DELETE',
      url: BACKEND_URL + `task/${button.target.id}/`,
      success: () => {
        $(`#${button.target.id}`).remove();
      },
      error: (e) => {
        console.log(e);
      }
    });
  });

  // task mark as done
  $(document).on('click', '.done', (button) => {
    $.ajax({
      type: 'PATCH',
      url: BACKEND_URL + `task/${button.target.id}/`,
      data: JSON.stringify({status: 'DO'}),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: () => {
        $(`#status-${button.target.id}`).text('status: ukończone');
        $('.done').remove();
      },
      error: (e) => {
        console.log(e);
      }
    });
  });

  $('#addTask').on('click', () => {
    $('#addTaskModal').modal('toggle');
  });

  $('#saveTask').on('click', () => {
    $.ajax({
      type: 'POST',
      url: BACKEND_URL + 'task/',
      data: JSON.stringify({
        assign_to: id,
        status: 'IP',
        description: $('#description').val(),
        category: $('#category').val(),
        deadline: $('#deadline').val()
      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: ({id, status, description, category, deadline}) => {
        addTask(id, status, description, categoryMap[category], deadline);
        $('#addTaskModal').modal('toggle');
      },
      error: (e) => {
        console.log(e);
      }
    });
  });

  // filtered tasks
  $('#filter').on('click', () => {
    var status = $('#status').val();
    var dateBefore = $('#before').val();
    var dateAfter = $('#after').val();
    var params = [];
    if(status) { params.push(`status=${status}`); }
    if(dateBefore) { params.push(`deadline__lte=${dateBefore}`); }
    if(dateAfter) { params.push(`deadline__gte=${dateAfter}`); }
    params = params.join('&');
    $.ajax({
      type: 'GET',
      url: BACKEND_URL + `employee-tasks/${id}/?${params}`,
      success: (tasks) => {
        console.log(tasks);
        $('.accordion-item').remove();
        $.each(tasks, (i, {id, status, description, category, deadline}) => {
          addTask(id, status, description, category, deadline);
        });
      },
      error: (e) => {
        console.log(e);
      }
    });
  });

  // export tasks to csv
  $('#exportCSV').on('click', () => {
    window.open(BACKEND_URL + `tasks-csv/${id}/`, '_blank');
  });

});