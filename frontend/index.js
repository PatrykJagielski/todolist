$(function() {
  const BACKEND_URL = 'http://localhost:8000/todo/';

  var $list = $('.list-group');
  var $button = $('#saveEmployee');

  const addEmployee = (id, firstName, lastName) => {
    var url = `/employee.html?id=${id}`;
    $list.append('<a class="list-group-item list-group-item-action" href=' + url + '>' + 
      `${firstName} ${lastName}` + '</a>');
  }

  $.ajax({
    type: 'GET',
    url: BACKEND_URL + 'employee/',
    success: (employees) => {
      $.each(employees, (i, {id, first_name, last_name}) => {
        addEmployee(id, first_name, last_name);
      });
    },
    error: (e) => {
      console.log(e);
    }
  });

  $button.on('click', () => {
    let url = BACKEND_URL + 'employee/';
    if($('#async').prop('checked')) {
      url = BACKEND_URL + 'async-add-employee/';
    }
    $.ajax({
      type: 'POST',
      url: url,
      data: JSON.stringify({
        first_name: $('#firstName').val(), 
        last_name: $('#lastName').val()
      }),
      contentType: "application/json; charset=utf-8",
      dataType: "json",
      success: (data) => {
        addEmployee(data.id, data.first_name, data.last_name);
        $('#addEmployee').modal('toggle');
      },
      error: (e) => {
        if(e.status === 202) {
          $('#addEmployee').modal('toggle');
        }
        else {
          console.log(e);
        }
      }
    });
  });
});